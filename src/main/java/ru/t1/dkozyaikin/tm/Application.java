package ru.t1.dkozyaikin.tm;

import ru.t1.dkozyaikin.tm.constant.TerminalConst;

public class Application {

    public static void main(String[] args) {
        if (args == null || args.length < 1) return;
        final String param = args[0];
        switch (param) {
            case TerminalConst.CMD_HELP: {
                displayHelp();
                break;
            }
            case TerminalConst.CMD_VERSION: {
                displayVersion();
                break;
            }
            case TerminalConst.CMD_ABOUT: {
                displayAbout();
                break;
            }
            default: {
                displayErrorArgument();
                break;
            }
        }
        System.exit(0);
    }

    private static void displayErrorArgument() {
        System.out.println("Error! This argument not supported...");
    }

    private static void displayHelp() {
        System.out.println("version - Display program version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of terminal commands.");
    }

    private static void displayVersion() {
        System.out.println("1.4.0");
    }

    private static void displayAbout() {
        System.out.println("Denis Kozyaykin");
        System.out.println("dkozyaikin@t1-consulting.ru");
    }

}
